from fun import StudentDB
from unittest import mock
import pytest

db = None # Setting up the variable db as global

mock_scott = mock.Mock() # setting up mock for Scott data
mock_mark = mock.Mock()  # setting up mock for Mark data


# setup module to connect the database only once
def setup_module(module):
    global db
    db = StudentDB()
    db.connect('data.json')
    print()
    print('Successfully connected to Database')


# teardown module to close the database connection when both test will run
def teardown_module(module):
    db.close()
    print()
    print('Connection Closed')


# test to check the scott datas
class TestExamples:
    def test_scott_data(self):
        scott_data = db.get_data('Scott')
        assert scott_data['id'] == 1
        assert scott_data['name'] == 'Scott'
        assert scott_data['result'] == "pass"
        # print("----------------------------Scott---------------------------------")
    
    def test_scott_mock(self):
        mock_scott.side_effect = ["Scott", 1, "pass"]
        assert mock_scott() == "Scott"
        assert mock_scott() == 1
        assert mock_scott() == "pass"
        # print("---------------------------Mock Scott passes successfully!------------------")

    def test_mark_mock(self):
        mock_mark.side_effect = ["Mark", 2, "fail"]
        assert mock_mark() == "Mark"
        assert mock_mark() == 2
        assert mock_mark() == "fail"
        # print("---------------------------Mock Mark passes successfully!------------------")

    # test to check the mark data
    def test_mark_data(self):
        mark_data = db.get_data('Mark')
        assert mark_data['id'] == 2
        assert mark_data['name'] == 'Mark'
        assert mark_data['result'] == "fail"
        # print("---------------------------Mark-----------------------------")
